const http = require("http");

const PORT = 4000;

const server = http.createServer((req, res) => {
  if (req.url === "/") {
    console.log(req.method);
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Welcome to Booking System");
  } else if (req.url === "/profile") {
    console.log(req.method);
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Welcome to your profile!");
  } else if (req.url === "/courses") {
    console.log(req.method);
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Here's our courses available");
  } else if ((req.url === "/addcourse", req.method === "POST")) {
    console.log(req.method);
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Add a course to our resources");
  } else if ((req.url === "/updatecourse", req.method === "PUT")) {
    console.log(req.method);
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Update a course to our resources");
  } else if ((req.url === "/archivecourses", req.method === "DELETE")) {
    console.log(req.method);
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Archive courses to our resources");
  }
});
server.listen(PORT);

console.log(`Server is running at localhost:${PORT}`);
